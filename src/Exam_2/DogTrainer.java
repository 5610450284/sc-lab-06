package Exam_2;

public class DogTrainer {
	public String makeDogBark (Pug pug){
		return "pug"+pug.bark();
	}
	
	public String makeDogBark(Shepherd shepherd){
		return "shepherd"+shepherd.bark();
	}
	
	public String makeDogBark(Dog dog){
		return "dog"+dog.bark();
	}
	
}
