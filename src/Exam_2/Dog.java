package Exam_2;

public abstract class Dog {
	private String name;
	public abstract String bark();
	
	public Dog (String name){
		this.name = name;
	}
	
	public String toString(){
		return this.name;
	}
}
