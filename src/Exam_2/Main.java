package Exam_2;

public class Main {
	public static void main(String[] args) {
		Pug pug = new Pug("PuPu");
		Shepherd shepherd = new Shepherd("PePe");
		DogTrainer dogTrainer = new DogTrainer();
		
		System.out.println(dogTrainer.makeDogBark(pug));
		System.out.println(dogTrainer.makeDogBark(shepherd));
		System.out.println(dogTrainer.makeDogBark((Dog) shepherd));
	}

}
